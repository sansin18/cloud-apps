package org.ashish;

import java.io.Serializable;

public class UserObj implements Serializable {

	private static final long serialVersionUID = 1L;
	private String emailId;
	private String designation;
	private String contactNum;
	private String firstName;
	private String lastName;
	private String resumeMetaId;
	private String profileMetaId;
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getContactNum() {
		return contactNum;
	}
	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getResumeMetaId() {
		return resumeMetaId;
	}
	public void setResumeMetaId(String resumeMetaId) {
		this.resumeMetaId = resumeMetaId;
	}
	public String getProfileMetaId() {
		return profileMetaId;
	}
	public void setProfileMetaId(String profileMetaId) {
		this.profileMetaId = profileMetaId;
	}


}
